#include <string>
#include <iostream>
#include <fstream>
#include <ctime>
#include <json/json.h>
#include <unistd.h>
#include <time.h>
#include <boost/format.hpp>
#include <boost/dll.hpp>
#include "../include/parse.hpp"
#include "../include/telegram.hpp"

std::tm *get_time_now()
{
    std::time_t time = std::time(nullptr);
    return std::localtime(&time);
}

int main()
{

    using boost::format;

    std::ifstream ifs;
    ifs.exceptions(std::ifstream::failbit);

    boost::filesystem::path exe_path =  boost::dll::program_location();
    std::string config_path = exe_path.parent_path().string() + "/config.json";

    try
    {

        ifs.open(config_path);
    }
    catch (std::ifstream::failure &e)
    {
        std::cout << "[Error] cannot read config.json" << std::endl;
        std::cout << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }

    Json::Value root;
    Json::Reader reader;

    reader.parse(ifs, root, false);

    std::string previous;
    std::string current;

    std::vector<std::string> req_fields = {"bot_token", "meet_url", "start_hour", "end_hour", "start_day", "end_day"};

    for(std::string f : req_fields){
        if (root[f].empty()){
            std::cout << "[Error] " << f << " is empty/NULL!" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    std::string token = root["bot_token"].asString();
    std::string meet_url = root["meet_url"].asString();
    int watch_start_hour = root["start_hour"].asInt();
    int watch_end_hour = root["end_hour"].asInt();
    int watch_start_day = root["start_day"].asInt();
    int watch_end_day = root["end_day"].asInt();

    std::cout << format("config.json: \n %s\n") % root;

    while (true)
    {

        char time_string[30];
        auto tm = get_time_now();
        strftime(time_string, 30, "%d/%m/%Y %I(%H):%M:%S", tm);

        if ((tm->tm_hour >= watch_start_hour && tm->tm_hour < watch_end_hour) &&
            (tm->tm_wday >= watch_start_day && tm->tm_wday <= watch_end_day))
        {

            try
            {
                current = get_current_meeting_title(meet_url);
            }
            catch (std::string &error)
            {
                std::cout << error << std::endl;
                sleep(5);
                continue;
            }

            if (previous.empty())
            {
                previous = current;
            }

            if (current != previous)
            {
                send_telegram_message(token, current, meet_url);
                previous = current;
            }

            std::cout << format("[%s] %s\n") % time_string % current;
            sleep(60 - tm->tm_sec);
        }

        else
        {
            unsigned int wait_time = ((59 - tm->tm_min) * 60) + 60 - tm->tm_sec;
            std::cout << format("[%s] time condition not met! sleeping for %u seconds!\n") % time_string % wait_time;
            sleep(wait_time);
        }
    }

    return 0;
}