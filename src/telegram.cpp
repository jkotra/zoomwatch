#include<iostream>
#include <vector>
#include <bits/stdc++.h>
#include "../include/tgbot/tgbot.h"

std::string escape_string(std::string message)
{

    std::string filtered;
    std::vector<char> escape_charecters = {'_',  '*', '[', ']', '(', ')', '~', '`', '>', '#', '+', '-', '=', '|', '{', '}', '.', '!'};

    for (char w : message)
    {
        auto it = std::find(escape_charecters.begin(), escape_charecters.end(), w);
        if (it != escape_charecters.end())
        {
            //std::cout << w << std::endl;
            filtered.append(std::string("\\") + w);
        }
        else
        {
            filtered.push_back(w);
        }
    }

    return filtered;
}


void send_telegram_message(std::string token, std::string update, std::string meet_url){

    TgBot::Bot bot(token);

    std::string message;

    message.append("⚠️ *ALERT* ⚠️");
    message.append("\nZoom meeting has been updated to:");
    message.append("\n\n*" + escape_string(update) + "*\n\n");
    message.append("🔗 [Join Meeting](" + escape_string(meet_url) + ")");

    std::cout << message << std::endl;


    bot.getApi().sendMessage("@c20zoom",
                            message,
                            false,
                            0,
                            std::make_shared<TgBot::GenericReply>(),
                            "MarkdownV2",
                            false);

}