#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <curl/curl.h>

#include "../include/parse.hpp"

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

std::string get_current_meeting_title(std::string meet_url){

    CURL *curl;
    CURLcode code;
    std::string readbfr;

    struct curl_slist *chunk = NULL;
    chunk = curl_slist_append(chunk, "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8");

    //chunk = curl_slist_append(chunk, "Accept-Encoding: gzip, deflate, br");
    chunk = curl_slist_append(chunk, "Accept-Language: en-US,en;q=0.5");
    chunk = curl_slist_append(chunk, "Connection: keep-alive");
    chunk = curl_slist_append(chunk, "Host: talentsprint.zoom.us");
    chunk = curl_slist_append(chunk, "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0");


    curl = curl_easy_init();
    if (curl){
        curl_easy_setopt(curl, CURLOPT_URL, meet_url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readbfr);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
        curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "br, gzip, deflate");
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        code = curl_easy_perform(curl);

        curl_slist_free_all(chunk);
        curl_easy_cleanup(curl);
        curl_global_cleanup();
    }

    if (code != 0){
        char error[50];
        sprintf(error, "%s %d", "[ERROR] cURL code:", code);
        throw( std::string(error) );
    }

    htmlDocPtr doc = htmlReadMemory(readbfr.c_str(), readbfr.size(), NULL, NULL, HTML_PARSE_NOBLANKS | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING | HTML_PARSE_NONET);
    xmlNode *root = xmlDocGetRootElement(doc);
    char* title = NULL;

    if ( (title = parse(root)) !=  NULL){
        std::cout << title;
        std::string s;
        s.assign(title);

        xmlFreeDoc(doc);

        return s;
    }
    else{

        xmlFreeDoc(doc);

        throw ( std::string("Unable to parse topic from readbfr!") );
    }

}

char* find_title(xmlNode *node){

    if (node == NULL){
        return NULL;
    }

    xmlNode *cur_node = NULL;
    char* title;

    for (cur_node = node; cur_node; cur_node = cur_node->next)
    {
        if (cur_node->type == XML_ELEMENT_NODE){
            if (strcmp((char *)cur_node->name, "strong") == 0){
                char *title = (char *)cur_node->children->content;
                return title;
            }
        }

        // check if recusive stack function is NULL
        if ( (title = find_title(cur_node->children)) != NULL){
            return title;
        }

    }

    return NULL;
}

char *parse(xmlNode *node)
{

    if (node == NULL){
        return NULL;
    }

    xmlNode *cur_node = NULL;
    char* title = NULL;

    for (cur_node = node; cur_node; cur_node = cur_node->next)
    {

        if (cur_node->type == XML_TEXT_NODE)
        {
            if (strcmp((char *)cur_node->content, "Topic") == 0)
            {
                xmlNode *topic = cur_node = cur_node->parent;
                return find_title(topic);
            }
        }
        
        // check if recusive stack function is NULL
        if ( (title = parse(cur_node->children)) != NULL){
            return title;
        }

    }

    return NULL;
}