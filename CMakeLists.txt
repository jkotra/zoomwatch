cmake_minimum_required(VERSION 3.0)
PROJECT(zoomwatch VERSION 1.1.0 LANGUAGES CXX)
set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -Wall -Wextra -std=c++17")

add_subdirectory(tgbot-cpp)

file(GLOB SRC_FILES ${PROJECT_SOURCE_DIR}/src/*.cpp)
add_executable(zoomwatch ${SRC_FILES})
target_link_libraries(zoomwatch TgBot)

if(WIN32)
    include_directories(include C:/msys64/mingw64/include/libxml2)
    target_link_libraries(zoomwatch ws2_32)
    set(Boost_USE_MULTITHREADED ON)
    target_link_libraries(zoomwatch libboost_system-mt)
    target_link_libraries(zoomwatch libboost_filesystem-mt)
endif()

if(UNIX)
    include_directories(include /usr/include /usr/include/jsoncpp/ /usr/include/libxml2/)
    target_link_libraries(zoomwatch boost_system)
    target_link_libraries(zoomwatch boost_filesystem)
    target_link_libraries(zoomwatch dl)
endif()

target_link_libraries(zoomwatch TgBot)
target_link_libraries(zoomwatch xml2)
target_link_libraries(zoomwatch ssl)
target_link_libraries(zoomwatch crypto)
target_link_libraries(zoomwatch pthread)
target_link_libraries(zoomwatch curl)
target_link_libraries(zoomwatch jsoncpp)

add_custom_command(
        TARGET zoomwatch POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
                ${CMAKE_SOURCE_DIR}/examples/config.json
                ${CMAKE_CURRENT_BINARY_DIR}/config.json)