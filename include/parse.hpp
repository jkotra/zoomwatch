#include <libxml2/libxml/HTMLparser.h>

std::string get_current_meeting_title(std::string meet_url);
char *parse(xmlNode *node);
char* find_title(xmlNode *node);